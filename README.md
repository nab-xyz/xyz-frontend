# Sample front-end using [react-admin](https://marmelab.com/react-admin/)

## Building the code and run application locally

- Install dependencies

```sh
npm i
```

- Start dev server:

```sh
npm start
```
