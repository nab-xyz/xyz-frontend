import React from 'react';
import {
  Datagrid,
  Filter,
  List,
  NumberField,
  SearchInput,
  NumberInput,
  TextField
} from 'react-admin';

const ProductFilter = (props: any) => (
  <Filter {...props}>
    <SearchInput source="name" alwaysOn={true} label="Name" />
    <SearchInput source="branch" label="Branch" />
    <SearchInput source="colour" label="Colour" />
    <NumberInput source="price_gte" label="Price From" />
    <NumberInput source="price_lte" label="Price To" />
  </Filter>
);

const ProductList = (props: { permissions: string }) => (
  <List
    {...props}
    filters={<ProductFilter />}
    sort={{ field: 'id', order: 'ASC' }}
    bulkActions={false}
  >
    <Datagrid>
      <TextField source="id" />
      <TextField source="name" />
      <TextField source="branch" />
      <TextField source="colour" />
      <NumberField source="price" options={{ style: 'currency', currency: 'USD' }} />
      <NumberField source="quantity" />
    </Datagrid>
  </List>
);

export default ProductList;
