import React, { useState, useEffect } from 'react';
import { Admin, Resource } from 'react-admin';

import './App.css';

import dataProviderFactory from './dataProvider';
import product from './product';

const App = () => {
  const [dataProvider, setDataProvider] = useState({});

  useEffect(() => {
    ;
    setDataProvider(() => dataProviderFactory({}));
  }, []);

  if (!dataProvider) {
    return (
      <div className="loader-container">
        <div className="loader">Loading...</div>
      </div>
    );
  }

  return (
    <Admin dataProvider={dataProvider}>
      <Resource key="resource-products" name="product/products" {...product} options={{ label: 'Products' }} />
    </Admin>
  );
}

export default App;
