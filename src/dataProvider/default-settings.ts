export default {
  headers: {
    Accept: 'application/hal+json; charset=utf-8',
    'Content-Type': 'application/json; charset=utf-8'
  }
};
